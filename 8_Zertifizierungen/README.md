# Zertifizierungen

*Aufzählung von Zertifizierungen, welche zusätzlich in diesem Modul (auch extern) erworben werden können.*

* [ITIL® 4 Foundation Kompaktkurs]()
* [ITIL® 4 Foundation Praxis](https://www.digicomp.ch/weiterbildung-service-projektmanagement/itil/kurs-itil-4-foundation-praxis)
  
  
*Siehe z.B. [https://www.digicomp.ch/zertifizierung](https://www.digicomp.ch/zertifizierung)*
