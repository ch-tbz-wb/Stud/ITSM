![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# ITSM - IT Service Management

## Kurzbeschreibung des Moduls

Es sollte das Ziel der IT sein, die Firma darin zu unterstützen, dass diese ihre Services optimal anbieten kann. Nichts anderes ist IT Service Management.

Nach dem Kurs sollten Sie Folgendes wissen:

- was Services überhaupt sind
- was Geschäftsprozesse mit Services zu tun haben
- woran bei IT Service Management gedacht werden sollte, damit die Services optimal angeboten werden können


## Angaben zum Transfer der erworbenen Kompetenzen

Umsetzung der erlernten Theorie in Einzel- und Gruppenarbeiten und bei einer Semesterarbeit.

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

* Geschäftsprozesse im 1. Sem.

### Nachfolgende Module

* Keine

## Dispensation

Gemäss Reglement HF Lehrgang

Zertifizierung "ITIL Foundation"

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung)

### Fragekatalog

[Fragekatalog](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - -
