# Handlungskompetenzen

## Allgemeine Handlungskompetenzen

 * A1 Unternehmens- und Führungsprozesse gestalten und verantworten
   * A1.12 Kundenbeziehungen gestalten (Niveau: 3)

 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten (Niveau: 3)
    * A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren (Niveau: 3)

## Modulspezifische Handlungskompetenzen

 * B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
   * B4.2 ICT-Problemstellungen unter Berücksichtigung vernetzten Denkens, Entwicklung neuer ICT-Lösungen und Anwendung aktueller Technologien identifizieren, analysieren und lösen (Niveau: 3)
   * B4.3 Kreative situativ passende ICT-Lösungen für komplexe Probleme mit ineinandergreifenden Einflussgrössen entwickeln (Niveau: 4)

 * B6 Eine ICT-Organisationseinheit leiten
   * B6.1 Aus dem Unternehmensleitbild und der ICT-Strategie die Anforderungen und Rahmenbedingungen ableiten und diese in der technischen ICT-Organisationseinheit konkret umsetzen (Niveau: 3)
   * B6.4 Informationstechnologien und -methoden, Marktinformationen und Umfeld (Konkurrenz, Forschung etc.) in der ICT beobachten und bewerten (Niveau: 3)* B6.5   Risiken einer ICT-Abteilung analysieren und geeignete Massnahmen ableiten (KN:3)

 * B7 Technische Anforderungen analysieren und bestimmen
   * B7.1 Die ICT-Architektur zielorientiert (ICT Strategie) analysieren, beurteilen und bestimmen (Niveau: 4)
   * B7.3 Technische Anforderungen aufnehmen und spezifizieren (Niveau: 3)
  * B7.5 Anforderungen für den Einsatz von Informatikmitteln spezifizieren (Niveau: 3)

 * B8 ICT-Qualität sicherstellen
   * B8.1 Das ICT-Qualitätsmanagement-System definieren, dokumentieren, umsetzen und überwachen (Niveau: 3)
   * B8.2 Optimierungen im Qualitätsmanagement planen und umsetzen (Niveau: 3)
   * B8.3 Qualitäts-Messsysteme mit Indikatoren festlegen (Niveau: 3)

 * B13 Konzepte und Services entwickeln
   * B13.4 Anforderungen aus dem Service-Management analysieren, entwickeln und integrieren (Niveau: 3)
   * B13.5 Service-Levels unter Berücksichtigung der Servicestrategie und Kundenvorgaben entwickeln (Niveau: 3)

 * B14 Konzepte und Services umsetzen
   * B14.3 Die Kundenzufriedenheit bezüglich ICTDienstleistungen durch Messungen undUmfragen ermitteln (Niveau: 2)
   * B14.5 ICT-Systeme und ICT-Dienstleistungen beschaffen (Niveau: 3)
   * B14.7 Verrechnungsmodell operativ erstellen, umsetzen und ICT- Dienstleistungen budgetieren und verrechnen (Niveau: 2)
 
## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
