# ITSM - IT Service Management

- Bereich: IT Service Management
- Semester: 2

## Lektionen

* Präsenzlektionen (Vor Ort): 40
* Präsenzlektionen (Distance Learning): 0
* Selbststudium: 20

## Lernziele

* Verständnis von IT Service Management (ITSM)
* Kenntnisse über ITIL4 Framework
* Anwendung von ITIL4 Prinzipien
* Service Value System (SVS)
* Service Value Chain (SVC)
* Kenntnisse über ITIL4 Prozesse und Praktiken
* Anwendung von Werkzeugen und Techniken
* Kontinuierliche Verbesserung

## Voraussetzungen

* Wissen über Geschäftsprozesse (Modul BPM)

## Dispensation

* Gemäss Reglement HF Lehrgang
* Zertifizierung "ITIL Foundation"

## Technik

GitLab Account, AWS Academy Umgebung, MAAS

## Methoden

Self-Learning Plattformen und praktische Laborübungen, Coaching durch Lehrperson

## Schlüsselbegriffe

* Service Management, Unternehmensprozesse, Geschäftsprozesse, Indikatoren
* Begriffe gemäss ITIL® 4: IT Service Management (ITSM), Service, Serviceangebot, Prozess, Aktivitäten, Rollen, die vier Dimensionen, Service Value System (SVS), ITIL Practices, ITIL-Grundprinzipien, Wertschöpfungskette, Continual Improvement, Governance, Stakeholder, Service Provider, Service Consumer, Utility & Warranty, Outputs & Ergebnisse, Kosten, Risiko, etc.

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel  

* [ITIL Foundation: ITIL® 4 Edition](https://www.amazon.de/ITIL-foundation-German-Translation/dp/0113316143)

## Hilfsmittel

* [IT-Service-Management in der Praxis mit ITIL®](https://www.amazon.de/Service-Management-Praxis-ITIL%C2%AE-Zusammenarbeit-systematisieren/dp/3446461868/ref=sr_1_2?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3ETRWZXU1WFS2&keywords=ITIL&qid=1667997454&s=books&sprefix=itil%2Cstripbooks%2C70&sr=1-2)
